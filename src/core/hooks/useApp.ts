/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import axios from "axios"
import { useEffect, useRef, useState } from "react"

export default function useApp() {
    const [loading, setLoading] = useState<boolean>(true)
    const [history, setHistory] = useState<any>([])
    // const [user] = useState<any>({ id: "1234", usuario: "user1" })
    const [ setSession] = useState<any>("1234")
    const [endpoint] = useState("https://APIGW-ID.execute-api.us-east-1.amazonaws.com")
    const chatBox = useRef<HTMLDivElement>(null)
    const chatWrp = useRef<HTMLDivElement>(null)

    const greeting = () => {
        const message = `Hola, Soy Cornita, la asistante inteligente de Unicorn Express!
        Podés conversar conmigo con tu voz, apretando el botón micrófono de abajo.`
        say(message).then().catch()
        setHistory([
            {
                id_usuario: "",
                id_session: "",
                human: false,
                message: message
            },
        ])
    }

    useEffect(() => {
        setTimeout(() => {
            setLoading(true)
            greeting()
            setLoading(false)
        }, 5000)
    }, [])


    useEffect(() => {
        axios.get(`/v1/handshake`, { baseURL: endpoint, })
            .then((res: any) => {
                const data = res.data
                if (data?.session) {
                    setSession(data.session)
                }
            })
    }, [])

    const say = async (_message: string) => {
        try {
            // const rachel = "xxxx"
            // const res = await fetch(`${elevenEndpoint}/v1/text-to-speech/${rachel}`, {
            //     method: "POST",
            //     headers: {
            //         Accept: "audio/mpeg",
            //         "xi-api-key": apikey,
            //         "Content-Type": "application/json"
            //     },
            //     body: JSON.stringify({
            //         "text": message,
            //         "model_id": "eleven_monolingual_v1",
            //         "voice_settings": {
            //             "stability": 0.5,
            //             "similarity_boost": 0.5
            //         }
            //     })
            // })
            // const buf = await res.blob()
            // if (buf) {
            //     const url = window.URL.createObjectURL(buf);
            //     const audio = new Audio(url)
            //     audio.play()
            // }
        } catch (error) {
            console.warn("say", error)
        }
    }



    const handlePrompt = ({ message }: any) => {
        console.log("handlePrompt", message)

        if (!message && loading) return

        const chatbox = document.getElementById("#chatbox")

        // const payload = { question: message }

        setHistory((prev: any) => [...prev, { message, human: true }])
        setLoading(true)

        if (chatbox) {
            chatbox.scroll({ top: chatbox.scrollHeight, behavior: "smooth" })
        }

        setTimeout(() => {
            const message = `Lo siento no puedo encontrar una respuesta por ahora,
                intentá de nuevo...seguro es un problema de conexión!`
            setHistory((prev: any) => [...prev, { message: message, human: false }])
            if (chatbox) {
                chatbox.scroll({ top: chatbox.scrollHeight, behavior: "smooth" })
            }
            say(message).then().catch()
            setLoading(false)
        }, 5000)
    }

    return { chatBox, chatWrp, history, loading, handlePrompt }
}
