/* eslint-disable @typescript-eslint/no-explicit-any */
import usetextInput from "./hooks";

export default function TextInput(props: any) {
    const { handleChange } = usetextInput()

    return (
        <div className='w-full h-full'>
            <textarea defaultValue={props.defaultValue}  onChange={handleChange} name={props.name} className=' border border-secondary rounded w-full h-full outline-none p-2'></textarea>
        </div>
    )
}
