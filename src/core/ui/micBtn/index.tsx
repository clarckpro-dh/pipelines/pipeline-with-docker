
import { BsMic } from "react-icons/bs";
import { BiMessageSquareDots } from "react-icons/bi";

import useMicBtn from "./hooks";

export default function MicBtn(props: any) {
    const { handleClick } = useMicBtn(props)
    return (
        <button
            disabled={props.disabled}
            type="button"
            onClick={handleClick}
            className="w-[32px] h-[32px] border-secondary bg-white flex items-center justify-center border rounded-full"
        >
            {props.disabled ? <BiMessageSquareDots size={14} /> : <BsMic size={14} />}
        </button>
    );
}
