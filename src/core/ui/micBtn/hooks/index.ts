/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState } from "react"

export default function useMicBtn(props:any){

    const [active, setActive] = useState(false)
    const handleClick = ()=>{
        setActive(!active)
        if (props.onClick) props.onClick()
    }
    return { active, handleClick }
}
