/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect } from 'react';
import { useState } from 'react';


export type TranscriberProps = {
    onMessage: (arg: { message: string }) => void
}

export default function useTranscriber(props: TranscriberProps) {
    const [transcript, setTranscript] = useState<any>()
    const [recognition, setRecognition] = useState<any>()
    const [recording, setRecording] = useState(false)
    const [busy] = useState(false)


    const handleRecognition = (ev: any) => {
        try {
            const transc = Array.from(ev.results)?.map((res: any) => res[0])?.map((res: any) => res.transcript)
            if (transc) {
                setTranscript(transc.join(" ").trim())
                console.log("transcript", transc)
            }
        } catch (err) {
            console.warn("handleRecognition", err)
        }
    }


    useEffect(() => {
        try {
            if (!window) return
            const recognition = new window.webkitSpeechRecognition() || new window.SpeechRecognition();
            recognition.interimResults = true
            recognition.continuous = true
            recognition.lang = "es-AR"
            // recognition.addEventListener('start', ()=>setBusy(true))
            // recognition.addEventListener('audioend', () => setBusy(false))
            // recognition.addEventListener('soundend', ()=>setBusy(false))
            // recognition.addEventListener('speechend', ()=>setBusy(false))
            recognition.addEventListener('result', handleRecognition)
            setRecognition(recognition)
        } catch (error) {
            console.warn("useEffect", error)
        }

    }, [])

    const handleMic = async () => {
        try {
            console.log("handle mic: recognition", recognition)
            if (!recording) {
                setRecording(true)
                setTranscript(() => "")
                return recognition.start()
            } else {
                recognition.stop()
                setRecording(false)
                const message = transcript
                setTranscript(() => "")
                if (message) {
                    props.onMessage({ message })
                }
            }
        } catch (error) {
            console.warn("handleMic", error)
        }
    }

    return { handleMic, busy, transcript }
}
