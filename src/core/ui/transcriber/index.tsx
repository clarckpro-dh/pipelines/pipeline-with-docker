import MicBtn from "../micBtn";
import TextInput from "../textInput";
import useTranscriber, { TranscriberProps } from "./hooks";

export default function Transcriber(props: TranscriberProps) {
    const { handleMic, busy, transcript } = useTranscriber(props)
    return (
        <div className='flex items-center gap-5 justify-between'>
            <div className='flex-1'>
                <TextInput defaultValue={transcript} />
            </div>
            <div className="">
                <MicBtn onClick={handleMic} disabled={busy} />
            </div>
        </div>
    )
}
