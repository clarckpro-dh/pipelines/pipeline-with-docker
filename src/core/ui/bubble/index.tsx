/* eslint-disable @typescript-eslint/no-explicit-any */
export default function Bubble({ human, message }: any) {
    return (
        <div className={["flex", human ? "justify-end" : ""].join(" ").trim()}>
            <div className={["p-3 border border-secondary bg-white  rounded max-w-[80%]"].join(" ").trim()}>
                <div>{message}</div>
            </div>
        </div>
    )
}
