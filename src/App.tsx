/* eslint-disable @typescript-eslint/no-explicit-any */
import './App.scss'
import useApp from './core/hooks/useApp'
import Bubble from './core/ui/bubble'
import Logo from './core/ui/logo'
import Transcriber from './core/ui/transcriber'

function App() {
  const { history, loading, handlePrompt } = useApp()
  return (
    <div className='h-full w-full relative'>
      <div className='w-[300px] min-w-[200px] h-[500px] border border-secondary bg-primary rounded-[10px]  flex flex-col text-xs absolute bottom-[5px] right-[5px]'>
        <div className='p-3 border-b border-white font-bold flex items-center justify-between'>
          <div>
            <Logo />
          </div>
          {/* <div className='flex items-center h-full px-2'>Unicorn Express</div> */}
        </div>
        <div id="chatbox" className='flex-1 p-5 overflow-auto relative'>
          <div className=' flex flex-col gap-5 '>
            {history?.map((item: any, key: number) => <Bubble key={key} message={item.message} human={item.human} />)}
          </div>
        </div>
        {loading && <div className='px-5 py-2'><div>escribiendo...</div></div>}
        <div className='p-5 pb-3 bg-white'>
          <Transcriber onMessage={handlePrompt} />
        </div>
      </div>
    </div>
  )
}

export default App
