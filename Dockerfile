FROM node:alpine as builder
WORKDIR /app 
COPY . . 
RUN npm install
RUN npm run build
#-------------
FROM nginx:stable-alpine3.17-slim as runner 
WORKDIR /usr/share/nginx/html
COPY --from=builder /app/nginx/ /etc/nginx/conf.d
COPY --from=builder /app/dist . 
ENTRYPOINT ["nginx", "-g", "daemon off;"]